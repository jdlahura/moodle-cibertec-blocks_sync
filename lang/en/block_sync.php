<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    block
 * @subpackage bcu_course_checks
 * @copyright  2014 Michael Grant <michael.grant@bcu.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */


// Strings for settings form.


$string['pluginname'] = 'Administración Cursos Hijos';
$string['configname'] = 'Sincronización de Cursos';
$string['titlename'] = 'Cursos Patrón';
$string['adminname'] = 'Dashboard';


$string['simplehtml'] = 'Simple HTML';
$string['simplehtml:addinstance'] = 'Add a new simple HTML block';
$string['simplehtml:myaddinstance'] = 'Add a new simple HTML block to the My Moodle page';
$string['see_father_son'] = 'Actualizar cursos Hijos';
$string['text'] = 'Prueba 11';
$string['form_text'] = 'Aqui se guardan cursos padres';
$string['form'] = 'Configuracion Plugin Padre Hijo';
$string['edit'] = 'Edicion Configuracion Plugin Padre Hijo';
$string['weeks'] = 'Semanal';
$string['onetopic'] = 'Pestañas';
$string['topics'] = 'topicos';
$string['social'] = 'social';
$string['section'] = 'Sección';
$string['feedback'] = 'Encuesta';
$string['forum'] = 'Foro';
$string['quiz'] = 'Cuestionario';
$string['url'] = 'url';
$string['resource'] = 'Archivo';
$string['assign'] = 'Tarea';

$string['scorm'] = 'Paquete Scorm';
$string['label'] = 'Etiqueta';
$string['certificate'] = 'Certificado';
$string['bootstraptabs'] = 'Formato Atypax';
$string['page'] = 'Página';

<?php
global $DB, $CFG;
include_once 'Classes/PHPExcel.php';
require_once("../../config.php");
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/modinfolib.php');
require_once($CFG->libdir.'/formslib.php');
//require_once('lib.php');

$id = required_param('id', PARAM_INT);
$courso = required_param('courseid', PARAM_INT);
  //$course = 3;
  //$id = 1;
  $coursename = get_course($courso);
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="Reporte Curso Patron - "'. $coursename->fullname  .'.xlsx"');
  header('Cache-Control: max-age=0');

  include_once 'Classes/PHPExcel.php'; 
  $phpexcel = new PHPExcel();
//==============================PAGINA 3==================================================== 

  $phpexcel->setActiveSheetIndex(0);  
  $objWorkSheet = $phpexcel->getActiveSheet()->setTitle('Resumen general');
  //Porcentaje de cincronizacion de cursos padres e hijos

  $prnt = "SELECT COUNT(sm.id) as cantidad_padres FROM {sync_main} sm";
  $parent = $DB->get_records_sql($prnt);

  $chld = "SELECT COUNT(rm.id) as cantidad_hijos FROM {sync_related} rm";
  $child = $DB->get_records_sql($chld);

  $snc = "SELECT * FROM {sync_user_history} suh
       ORDER BY suh.main_id ASC, suh.time_sync DESC ";
       
  $sync = $DB->get_records_sql($snc);

  $children = array();
  $parents = array();
  $temp = '';

  foreach ($sync as $key => $value) {
     if ($temp == $value->main_id) {
      unset($sync[$key]);
     }
    $temp =  $value->main_id;
  }

  foreach ($sync as $key => $value) {
    array_push($parents, $value->main_id);

    $listchl = explode(',', $value->child_id);
    foreach ($listchl as $keys => $values) {
      if ($values == '') {
        continue;
      }
      array_push($children, $values);
    }
  }
  $hijos = key($child);
  $padres = key($parent);
  $child =  (count($children)/$hijos)*100;
  $child =  number_format(round($child,2), 2, '.', '');
  $parent = (count($parents)/$padres)*100;
  $parent =  number_format(round($parent,2), 2, '.', '');
  //FIN Porcentaje de cincronizacion de cursos padres e hijos
  $objWorkSheet->getRowDimension(1)->setRowHeight(25);
  $objWorkSheet->getColumnDimension('A')->setAutoSize(true);
    $objWorkSheet->getColumnDimension('B')->setAutoSize(true);
    $objWorkSheet->getColumnDimension('C')->setAutoSize(true);
    $objWorkSheet->getColumnDimension('D')->setWidth(35);
    $objWorkSheet->getColumnDimension('E')->setAutoSize(true);
    $styleArray = array(    
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      ),
      'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
      ));
    $objWorkSheet->getStyle('A1:D3')->applyFromArray($styleArray);
    $objWorkSheet->getStyle('A1:L400')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objWorkSheet->getStyle('A1:D1')->applyFromArray(
          array(
              'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '3f8cce')
              )
          ));
    $objWorkSheet->getStyle("A1:D1")->getFont()->setBold(true);
     
     /*$objWorkSheet->getStyle('A2:A3')->applyFromArray(
          array(
              'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '66a7de')
              )
          ));*/
    

  $objWorkSheet->setCellValueByColumnAndRow(0,1, 'CURSOS');
  $objWorkSheet->setCellValueByColumnAndRow(0,2, 'CURSOS PADRES');
  $objWorkSheet->setCellValueByColumnAndRow(0,3, 'CURSOS HIJOS');
  $objWorkSheet->setCellValueByColumnAndRow(1,2, $padres);
  $objWorkSheet->setCellValueByColumnAndRow(1,3, $hijos);
  $objWorkSheet->setCellValueByColumnAndRow(3,2, $parent.'%');
  $objWorkSheet->setCellValueByColumnAndRow(3,3, $child.'%');
  
  $objWorkSheet->setCellValueByColumnAndRow(3,1, 'Porcentaje de cursos sincronizados');//pasar a  la D

  $objWorkSheet->setCellValueByColumnAndRow(2,1, 'Cursos sincronizados');
  $objWorkSheet->setCellValueByColumnAndRow(1,1, 'Cantidad Total');
  //$objWorkSheet->setCellValueByColumnAndRow(3,1, 'Cursos Padres sincronizados al 100 %'.PHP_EOL);
  $chlsync = $hijos * ($child/100);
  $objWorkSheet->setCellValueByColumnAndRow(2,2, count($parents));
  $objWorkSheet->setCellValueByColumnAndRow(2,3, round($chlsync,0));

//====================FIN PAGINA 3=============================

//==============================PAGINA 1====================================================

  //$sheet = $phpexcel->getActiveSheet()->setTitle('Reporte Curso Patron');
  $sheet = $phpexcel->createSheet()->setTitle('Reporte Curso Patron');
  $sheet->getColumnDimension('A')->setWidth(30);
  $sheet->getColumnDimension('B')->setWidth(30);
  $sheet->getColumnDimension('C')->setAutoSize(true);
  $sheet->getColumnDimension('D')->setAutoSize(true);
  $sheet->getColumnDimension('E')->setAutoSize(true);
  $sheet->getColumnDimension('F')->setAutoSize(true);
  $sheet->getColumnDimension('G')->setAutoSize(true);
  //$sheet->getColumnDimension('H')->setAutoSize(true);  
  $sheet->mergeCells('A2:B2');
  $sheet->getStyle('A1:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('A3:L400')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle("A2:G2")->getFont()->setBold(true);
  $sheet->getStyle('A2:G2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '3f8cce')
            )
        )); 

  /*$sheet->getStyle('I2:L2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '3f8cce')
            )
        ));*/
  $styleArray = array(    
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    )
  );

 
  $sheet->getRowDimension(2)->setRowHeight(40);

   $curso = "SELECT suh.id, suh.main_id, suh.child_id FROM {sync_user_history} suh
           WHERE suh.main_id in (?)
           ORDER BY suh.main_id ASC, suh.time_sync DESC";
  //$cursos = $DB->get_records_sql($curso,array($courseid));
  $cursos = $DB->get_records_sql($curso,array($courso));

  $synctimes = count($cursos);

  //##################FUNCION SEGUIR PROGRESO#######################
  function sync_check_course($mainid,$courseid){
    global $DB;
    //$output = array();
    $modules = $DB->get_records('sync_modules',  array('main_id' => $mainid));
    $cant_create = 0;
    $cant_update = 0;
    $cant_delete = 0;
    $cant_total = 0;
      $object = new stdClass();
      $object->message = '';
      $cant_total = count($modules);
    foreach ($modules as $module) {
      $instance = null;
      $exists = $DB->get_record('sync_modules_course',array('smodule_id'=>$module->id,'course_id'=>$courseid));
      $list = $DB->get_records_menu('modules',array(),null,'id,name');
      $entity = $DB->get_record('course_modules',array('id'=>$module->module_id));

      if($entity){
        $instance = $DB->get_record($list[$entity->module],array('id'=>$entity->instance));
      }

      if(!$exists){
        if($entity){
          $cant_create++;
          $object->message .= html_writer::tag('p','Crear actividad: '. $instance->name);
        }
      }else{
        $course_entity = $DB->get_record('course_modules',array('id'=>$exists->module_id));
        if($course_entity){
          $course_instance = $DB->get_record($list[$course_entity->module],array('id'=>$course_entity->instance));
        }

        //If the module still there
        if(isset($instance)){

          if(isset($course_instance)){


            if($instance->timemodified > $course_instance->timemodified || $entity->visible != $course_entity->visible){
              $cant_update++;
              $object->message .= html_writer::tag('p','La actividad: '. $instance->name.' debe ser actualizada');
            }

          }else{
            $cant_create++;
            $object->message .= html_writer::tag('p','Crear actividad: '. $instance->name);
          }

        }else{

          if(isset($course_instance)){
            $cant_delete++;
            
            $object->message .= html_writer::tag('p','La actividad: '. $course_instance->name.' debe ser eliminada');

          }
        }

      }
      
    }
    //$output[] = $object;
    $percent = 100 - round(($cant_create+$cant_update+$cant_delete) / $cant_total * 100, 0);
    //$percent = (($cant_create+$cant_update+$cant_delete)/$cant_total) * 100;

    $output =array(
                    'creates' => $cant_create,
                    'updates' => $cant_update,
                    'deletes' => $cant_delete,
                    'total' => $cant_total,
                    'percent' => $percent,
                    );


    return $output;

  }
  //#########################################################
  $temp = array_shift($cursos);
  $cursos = array();
  array_push($cursos, $temp);
  $ids = array();

  foreach ($cursos as $key => $value) {
     $ids[$value->main_id] =  $value->main_id;
     $child = explode(',', $value->child_id);
     array_pop($child);
     foreach ($child as $value) {
        $ids[$value] =  $value;
     }
  }
  $cont = 0;
  $crss = array();

  $title = array('Reporte de Monitoreo de Implementación'.PHP_EOL,'CURSO'.PHP_EOL,'Nro Secciones'.PHP_EOL, 'Formato de Curso'.PHP_EOL, 'Coordinador a Cargo '.PHP_EOL, 'Porcentaje de Sincronización'.PHP_EOL, 'Elementos creados en curso hijo'.PHP_EOL);
  $td=0;

  foreach ($title as $key => $value) {
      $sheet->setCellValueByColumnAndRow($td,2, $value);
      $td++;
  }

     $cont = 1;
     $tr1 = 3;

  foreach ($ids as $key => $value) {
     $coord = '';
     $mdsec = '';
     $coordinador = "SELECT CONCAT(u.firstname,' ', u.lastname) as coordinador FROM {course} c
                 INNER JOIN {context} ctx ON ctx.instanceid = c.id
                 INNER JOIN {role_assignments} ra ON ctx.id = ra.contextid
                 INNER JOIN {role} r ON r.id = ra.roleid
                 INNER JOIN {user} u ON u.id = ra.userid
                 WHERE r.id = 12 and c.id IN (?)";
     $coordinadores = $DB->get_records_sql($coordinador, array($value));           
     foreach ($coordinadores as $ke => $valu) {
        $coord = $valu->coordinador;     
     }
     $dato = "SELECT c.id, c.shortname,  COUNT(cs.section) as sections, c.format as formato
          FROM {course} c 
          INNER JOIN {course_sections} cs ON c.id = cs.course
          where c.id IN (?) 
          GROUP BY c.shortname";

     $datos = $DB->get_records_sql($dato, array($value));
      
      $titulos2 = array();
      $modschld = array();

      foreach ($datos as $key => $value) {
          $value->coordinador = $coord;
          $percent = sync_check_course($id,$value->id);
          /*echo "<pre>";
          print_r($percent);
          echo "</pre>";*/
          $value->porcentaje = $percent['percent'];
          $sheet->getStyle('A2:G'.$tr1)->applyFromArray($styleArray);
          
          $sheet->getStyle('A3:A'.$tr1)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '66a7de')
                )
            )); 

          if ($value->id == $courso) {
             $crs = 'Padre';
             $value->tipo = $crs;
          }else{
             $crs = 'Hijo ' . $cont;
             $value->tipo = $crs;
             $cont++;
          }

          if ($value->id != $courso) {
            
            $moduC = "SELECT cm.id, m.name as modname, cs.section, cm.course FROM {course_modules} cm
                           INNER JOIN {modules} m ON m.id = cm.module
                           INNER JOIN {course_sections} cs ON cs.id = cm.section
                           WHERE cm.course IN (?)
                           ORDER BY m.name ASC";
               $modulC = $DB->get_records_sql($moduC, array($value->id));
               
               $modulesC = $DB->get_records('sync_modules_course',array("course_id"=>$value->id),null,'module_id');

                 
               $chlonly = array_keys($modulC);
               foreach ($chlonly as $key => $chld) {
                 if (in_array($chld, array_keys($modulesC))) {
                   unset($modulC[$chld]);            
                 }
               }

              /**
                * count all modules creates in child course
                */ 
                $modschld[$value->id] = count($modulC);

               //##CONTAR POR TIPO DE MODULO  
               //$contm = 0;
               //$tmpn = '';
               //$tmpc = array();
               /*foreach ($modulC as $cle => $valeur) {
                   
                  if ($value->id == $valeur->course) {
                      $contm++;
                      if ($valeur->modname != $tmpn) {
                         $contm = 1;
                      }                  
                      $tmpc[$valeur->modname]= $contm;
                      $tmpn = $valeur->modname;
                  }

                }
                $modschld[$value->id] = $tmpc;*/           
          }

      }
                /*echo "<pre>";
                  print_r($modschld);
                echo "</pre>";*/


      $tm = array();
      if (empty($modschld)) {
        $tm[$courso] = '';
      }else{

        foreach ($modschld as $ll => $val) {    
          $contn = '';
          if ($val == 0) {
            $contn = 'Elementos sincronizados correctamente';
          }else{
            $contn = $val;
          }
          $tm[$ll] =$contn;
          //echo $contn;
        }

        //##IMPRIMIR CONTEO POR TIPO DE MODULO
        /*foreach ($modschld as $ll => $val) {    
          $contn = '';
          if ($val == array()) {
            $contn = 'Elementos sincronizados correctamente';
          }else{
            foreach ($val as $modle => $mhijo) {
              $contn .= '-'.get_string($modle, 'block_sync') . ' => ' .$mhijo.PHP_EOL;
            }
          }
          $tm[$ll] =strtoupper($contn);
          //echo $contn;
        }*/
      }

      /*echo "<pre>";
                  print_r($tm);
                echo "</pre>";*/
         foreach ($datos as $key => $dato) {
            $formatos = get_string($dato->formato);
            $sheet->getRowDimension($tr1)->setRowHeight(30);
            $sheet->setCellValueByColumnAndRow(0,$tr1, $dato->tipo);
            $sheet->setCellValueByColumnAndRow(1,$tr1, $dato->shortname);
            $sheet->setCellValueByColumnAndRow(2,$tr1, $dato->sections);
            $sheet->setCellValueByColumnAndRow(3,$tr1, get_string($dato->formato, 'block_sync'));
            $sheet->setCellValueByColumnAndRow(4,$tr1, $dato->coordinador);
            $sheet->setCellValueByColumnAndRow(6,$tr1,  $tm[$key]);
            //$sheet->setCellValueByColumnAndRow(5,$tr1, $dato->coordinador);
            //$sheet->setCellValueByColumnAndRow(5,$tr1, round($dato->porcentaje,2).'%'); 
            $redond = number_format(round($dato->porcentaje,2), 2, '.', '');  
             if ($value->id == $courso) {
              $sheet->setCellValueByColumnAndRow(5,$tr1, '');
             }else{
                $sheet->setCellValueByColumnAndRow(5,$tr1, round($redond,2).'%');
             }       
            $tr1++;
         }    
    
  }
  $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4)->setPrintArea('A2:G'.$tr1);
  //die();
  /*
  $title2 = array('Usuarios'.PHP_EOL,'Cursos sincronizados'.PHP_EOL,'Fecha'.PHP_EOL, 'Numero de Sincronización'.PHP_EOL);
  $td2 = 8;
  $tr2 = 3;
  $fecha = '';

  foreach ($title2 as $key => $value) {
      $sheet->setCellValueByColumnAndRow($td2,2, $value);
      $td2++;
  }


  $syncuser = "SELECT suh.user_id FROM {sync_user_history} suh WHERE suh.main_id IN (?) group by suh.user_id";
  $syncusers = $DB->get_records_sql($syncuser,array($courso));
  $userdata = array();

  $cont = 0;
  foreach ($syncusers as $key => $value) {

     $user_logs = $DB->get_records('sync_user_history',  array('main_id' => $courso, 'user_id' => $value->user_id)); 


     $usuario = $DB->get_record('user',  array('id' => $value->user_id));
     //$userpicture = $OUTPUT->user_picture($usuario,array('size' => 50));
     //$userurl = new moodle_url('/user/view.php', array('id' => $usuario->id));

     foreach ($user_logs as $values) {
      $cont++;
      $courses = explode(',', $values->child_id);
      $out_courses = '';
      $sheet->getStyle('I2:L'.$tr2)->applyFromArray($styleArray);  
      $sheet->getStyle('J'.$tr2)->getAlignment()->setWrapText(true);  
      $sheet->getRowDimension($tr2)->setRowHeight(50);
      $sheet->getStyle('I3:I'.$tr2)->applyFromArray(
          array(
              'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '66a7de')
              )
          )); 
        if(count($courses) >= 2){
           foreach ($courses as $val) {
              if($val != ''){
                 $coursee = get_course($val);
                 $out_courses .= '-'.$coursee->fullname.PHP_EOL;
              }
           }
        }
        $fecha = gmdate("Y-m-d H:i:s", $values->time_sync);
        $sheet->setCellValueByColumnAndRow(8,$tr2, fullname($usuario));
        $sheet->setCellValueByColumnAndRow(9,$tr2, $out_courses);
        $sheet->setCellValueByColumnAndRow(10,$tr2, $fecha);
        $sheet->setCellValueByColumnAndRow(11,$tr2, $cont);
        $tr2++;
        //$table_users->data[] = array($out_courses,gmdate("Y-m-d H:i:s", $values->time_sync), $cont);      
     }
  }  */

//==============================FIN PAGINA 1==================================================== 
//====================PAGINA 2============================= 
  $sheet3 = $phpexcel->createSheet()->setTitle('Reporte de Usuario');  
  $title3 = array('Usuarios'.PHP_EOL,'Cursos sincronizados'.PHP_EOL,'Fecha'.PHP_EOL, 'Numero de Sincronización'.PHP_EOL);
    $sheet3->getColumnDimension('A')->setAutoSize(true);//setWidth(20);  
    $sheet3->getColumnDimension('B')->setAutoSize(true);
    $sheet3->getColumnDimension('C')->setAutoSize(true);  
    $sheet3->getColumnDimension('D')->setAutoSize(true);
    $sheet3->getStyle('A1:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $sheet3->getStyle('A3:L400')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $sheet3->getStyle("A2:D2")->getFont()->setBold(true);
    $sheet3->getStyle('A2:D2')->applyFromArray(
          array(
              'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '3f8cce')
              )
          ));
   
    /*$sheet->getStyle('I2:L2')->applyFromArray(
          array(
              'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '3f8cce')
              )
          ));*/
    $styleArray = array(    
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      ),
      'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
      ));

  $td2 = 0;
  $tr2 = 3;

  $fecha = '';

  foreach ($title3 as $key => $value) {
      $sheet3->setCellValueByColumnAndRow($td2,2, $value);
      $td2++;
  }


  $syncuser = "SELECT suh.user_id FROM {sync_user_history} suh WHERE suh.main_id IN (?) group by suh.user_id";
  $syncusers = $DB->get_records_sql($syncuser,array($courso));
  $userdata = array();

  $cont = 0;
  foreach ($syncusers as $key => $value) {

     $user_logs = $DB->get_records('sync_user_history',  array('main_id' => $courso, 'user_id' => $value->user_id)); 


     $usuario = $DB->get_record('user',  array('id' => $value->user_id));
     //$userpicture = $OUTPUT->user_picture($usuario,array('size' => 50));
     //$userurl = new moodle_url('/user/view.php', array('id' => $usuario->id));

     foreach ($user_logs as $values) {
      $cont++;
      $courses = explode(',', $values->child_id);
      $out_courses = '';
      $sheet3->getStyle('A2:D'.$tr2)->applyFromArray($styleArray);  
      $sheet3->getStyle('B'.$tr2)->getAlignment()->setWrapText(true);  
      $sheet3->getRowDimension(2)->setRowHeight(40);
      $sheet3->getRowDimension($tr2)->setRowHeight(80);
      $sheet3->getStyle('A3:A'.$tr2)->applyFromArray(
          array(
              'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => '66a7de')
              )
          )); 
        if(count($courses) >= 2){
           foreach ($courses as $val) {
              if($val != ''){
                 $coursee = get_course($val);
                 $out_courses .= '-'.$coursee->fullname.PHP_EOL;
              }
           }
        }
        $fecha = gmdate("Y-m-d H:i:s", $values->time_sync);
        $sheet3->setCellValueByColumnAndRow(0,$tr2, fullname($usuario));
        $sheet3->setCellValueByColumnAndRow(1,$tr2, $out_courses);
        $sheet3->setCellValueByColumnAndRow(2,$tr2, $fecha);
        $sheet3->setCellValueByColumnAndRow(3,$tr2, $cont);
        $tr2++;
        //$table_users->data[] = array($out_courses,gmdate("Y-m-d H:i:s", $values->time_sync), $cont);      
     }
  }   


//====================FIN PAGINA 2=============================  



  $writer = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
  $writer->setIncludeCharts(TRUE);
  $writer->save('php://output');

<?php
global $DB, $PAGE, $OUTPUT;

require_once("../../config.php");
require_once($CFG->libdir.'/adminlib.php');
include('forms.php');
include('lib.php');
// Input params

admin_externalpage_setup('blocksync');

$context = context_system::instance();

require_login();

require_capability('block/sync:config',$context);

$main_url = new moodle_url('/blocks/sync/admin.php');

$PAGE->set_context($context);
$PAGE->set_url($main_url);
$title = 'Listado de Relaciones';
$PAGE->set_title($title);
$PAGE->set_heading($title);



  
  $mform = new sync_clear_like($main_url . '');

  if ($data = $mform->get_data()) {

    
    $sql = "SELECT * FROM {course} WHERE shortname LIKE '%" . $data->value . "%'";

    $courses = $DB->get_records_sql($sql);
   /* echo "<pre>";
    print_r($courses);
    echo "</pre>";*/

    if($courses == array()){
      redirect(new moodle_url('/blocks/sync/admin.php'), 'No se encontró Cursos Hijo con ese valor', 3);
    }


    $_SESSION['wppa'] = true;  
    $_SESSION['dataatxsync'] = $data->value;


    
  }else if(!isset($_SESSION['wppa'])){

    if ($mform->is_cancelled()) {
      $returnurl = new moodle_url('/blocks/sync/admin.php');
      redirect($returnurl);
    } else{
      print $OUTPUT->header();
      $mform->display();
      print html_writer::empty_tag('hr');

      $url = new moodle_url('/blocks/sync/create_main.php');
      $text = 'Nueva Relación'; //Translate this
      print html_writer::link($url,$text,array('class'=>'btn btn-default'));
      print html_writer::empty_tag('br');

      $table = new html_table();
      $table->head = array('Curso Padre','Cursos Hijos','');
      $courses = $DB->get_records_menu('course',array(),null,'id,shortname');  

      $records = $DB->get_records('sync_main');
      $table->data = array();

      foreach($records as $r){
        $line = array();
        $line[] = $courses[$r->courseid];

        $childs =  $DB->get_records('sync_related',array('main_id'=>$r->id));

        $l = array();
        foreach($childs as $c){
          $l[] = html_writer::tag('p',$courses[$c->courseid]);
        }

        $line[] = implode('', $l);


        $links = '';
        $url = new moodle_url('/blocks/sync/edit_main.php',array('id'=>$r->id));
        $text = 'Editar'; //Translate this
        $links .= html_writer::link($url,$text,array('class'=>'btn btn-default'));

        $url = new moodle_url('/blocks/sync/delete_main.php',array('id'=>$r->id));
        $text = 'Eliminar'; //Translate this
        $links .= html_writer::link($url,$text,array('class'=>'btn btn-default'));

        $url = new moodle_url('/blocks/sync/clear_main.php',array('id'=>$r->id));
        $text = 'Limpiar Hijos'; //Translate this
        $links .= html_writer::link($url,$text,array('class'=>'btn btn-default'));

        $line[] = $links;
        $table->data[] = $line;
      }

      echo html_writer::table($table);
      
    }

  }
  if(isset($_SESSION['wppa']) && $_SESSION['wppa'] == true){ 
    $mform2 = new sync_like_params($main_url, array('key' => $_SESSION['dataatxsync'] ));
  }

  if (isset($mform2) && $mform2->is_cancelled()) {
    unset($_SESSION['wppa']);
    $returnurl = new moodle_url('/blocks/sync/admin.php');
    redirect($returnurl);
  } else if (isset($mform2) && $segunda = $mform2->get_data()){

    $segunda = (array)$segunda;
    /*echo "---------------------------<pre>";
    print_r($segunda);
    echo "</pre>";*/
    unset($segunda['submitbutton']);
    foreach ($segunda as $key => $value) {

      if($value == 0){
        continue;
      }
      //$tmp = $DB->get_record('sync_main',  array('courseid' => $key));
      $tmp = $DB->get_record('sync_related',  array('courseid' => $key));

      /*echo "<pre>";
      print_r($tmp);
      echo "</pre>";*/
      
      if(!is_object($tmp)){
        continue;
      }
      

      //$DB->delete_records('sync_main', array('courseid'=>$key));
      $DB->delete_records('sync_related', array('courseid'=>$key));  
      //$DB->delete_records('sync_modules', array('main_id'=>$tmp->id));
      $DB->delete_records('sync_modules_course', array('course_id'=>$key));  
      //$DB->delete_records('sync_related', array('main_id'=>$tmp->id));        


      //actualizar sync history
      $mainid = $DB->get_record('sync_main',  array('id' => $tmp->main_id));
      $chllist = $DB->get_records('sync_user_history',  array('main_id' => $mainid->courseid));
      
      if ($chllist == array()) {
        continue;
      }

      $chlids = array_pop($chllist);

       $tem = explode(',', $chlids->child_id);

       foreach ($tem as $ke => $value) {
         if ($value == $key) {
           unset($tem[$ke]);
         }
       }
       $hijos = '';
       foreach ($tem as $ke => $valu) {
         if ($valu == '') {
           continue;
         }
         $hijos  .= $valu .',';
        //echo $hijos;
       }


       $update = new stdClass();
       $update->id = $chlids->id ;
       $update->child_id = $hijos ;
       $DB->update_record('sync_user_history',  $update);

      /*echo "<pre>";
      print_r($update);
      echo "</pre>";
      echo $key;*/

      
    }

    unset($_SESSION['wppa']); 
    $returnurl = new moodle_url('/blocks/sync/admin.php');
    redirect($returnurl);
    print $OUTPUT->header();


  }

  if(isset($_SESSION['wppa']) && $_SESSION['wppa'] == true){
    print $OUTPUT->header();
    echo html_writer::tag('h3','Cursos por eliminar relación');
    $mform2->display();
  }



print $OUTPUT->footer();

